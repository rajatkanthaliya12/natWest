import React, { useEffect, useState } from "react";

import "bootstrap/dist/css/bootstrap.min.css";
import "./app.scss";
import PaymentTable from "./paymentTable";
import LoadMoreButton from "./loadMoreButton";

const App = () => {
  const API_BASE_URL = "http://localhost:9001/api/payments";

  const [paymentData, setPaymentData] = useState([]);
  const [loadMoreData, setLoadMoreData] = useState({});
  const [loading, setLoading] = useState(false);

  const fetchPaymentInfo = async () => {
    setLoading(true);
    let url = Object.keys(loadMoreData).length
      ? `${API_BASE_URL}?pagelndex=${loadMoreData.nextPageIndex}`
      : API_BASE_URL;
    let response = await fetch(url);
    response = await response.json();
    const payments = [...paymentData, ...response.results];
    setPaymentData(payments);
    setLoadMoreData(response.metaDatal);
    setLoading(false);
  };

  useEffect(() => {
    fetchPaymentInfo();
  }, []);

  return (
    <div className="container mt-4">
      <PaymentTable paymentData={paymentData} />
      {loadMoreData.hasMoreElements && (
        <LoadMoreButton
          loadMoreData={loadMoreData}
          loading={loading}
          fetchMoreData={fetchPaymentInfo}
        />
      )}
    </div>
  );
};

export default App;
