import React from "react";
import Button from "react-bootstrap/Button";
import Spinner from "react-bootstrap/Spinner";

const LoadMoreButton = ({ loadMoreData, loading, fetchMoreData }) => {
  return (
    <div className="container mt-4">
      {loadMoreData.hasMoreElements && (
        <Button
          disabled={loading}
          onClick={fetchMoreData}
          className="my-3"
          variant="secondary"
        >
          {loading && (
            <Spinner
              as="span"
              animation="border"
              size="sm"
              role="status"
              aria-hidden="true"
            />
          )}
          {loading ? "loading" : "Load More"}
        </Button>
      )}
    </div>
  );
};

export default React.memo(LoadMoreButton);
