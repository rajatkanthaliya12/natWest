import React from "react";
import Table from "react-bootstrap/Table";

const PaymentTable = ({ paymentData }) => {
  const getPaymentStatus = (status) => {
    let statusInfo = "";
    switch (status) {
      case "A":
        statusInfo = "Approved";
        break;
      case "C":
        statusInfo = "Cancelled";
        break;
      case "P":
        statusInfo = "Pending Approval)";
        break;
    }
    return statusInfo;
  };

  return (
    <Table striped bordered hover size="sm">
      <thead>
        <tr>
          <th>#</th>
          <th>To Account Name</th>
          <th>From Accoount Name</th>
          <th>Amount</th>
          <th>Payment Type</th>
          <th>Payment Date</th>
          <th>Status</th>
        </tr>
      </thead>
      <tbody>
        {paymentData.map((data, i) => {
          return (
            <tr key={i}>
              <td>{i + 1}</td>
              <td>{data.toAccaunt.accountName}</td>
              <td>{data.fromAccount.accountName}</td>
              <td>{data.paymentAmount}</td>
              <td>{data.paymentType}</td>
              <td>{data.paymentDate}</td>
              <td>{getPaymentStatus(data.paymentStatus)}</td>
            </tr>
          );
        })}
      </tbody>
    </Table>
  );
};

export default React.memo(PaymentTable);
